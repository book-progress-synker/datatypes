use chrono::{DateTime, FixedOffset, Local, ParseError};
// use rand::Rng;
use serde::{Deserialize, Serialize};
use thiserror::Error;
// use uuid;

#[derive(Error, Debug)]
pub enum BookErr {
    #[error("time parse error try use \"%Y-%m-%d %H:%M:%S.3f %z\" format")]
    TimeParse(#[from] ParseError),
    #[error("failed convert to progress structure")]
    Progress(#[from] ProgressErr),
    // #[error("in that case id shouldn't be None")]
    // IdIsNone,
    #[error("unknown data store error")]
    Unknown,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct BookBuilder {
    // device: ,
    // hashsum (?) not sure because will be need to calculate for every book
    // id: Option<String>,
    filename: String,
    progress: f64,
    last_opened: String,
}

impl BookBuilder {
    pub fn new(// id: String,
        filename: String, progress: f64, last_opened: String) -> BookBuilder {
        BookBuilder {
            // id: Some(id),
            filename,
            progress,
            last_opened,
        }
    }

    /// id will be generated on build stage
    pub fn new_without_id(filename: String, progress: f64, last_opened: String) -> BookBuilder {
        BookBuilder {
            // id: None,
            filename,
            progress,
            last_opened,
        }
    }

    // /// just in case
    // pub fn new_option_id(
    //     // id: Option<String>,
    //     filename: String,
    //     progress: f64,
    //     last_opened: String,
    // ) -> BookBuilder {
    //     match id {
    //         Some(id) => Self::new(id, filename, progress, last_opened),
    //         None => Self::new_without_id(filename, progress, last_opened),
    //     }
    // }

    // pub fn build_with_existing_id(&self) -> Result<Book, BookErr> {
    //     if self.id.is_none() {
    //         return Err(BookErr::IdIsNone)
    //     }
    //     self.build()
    // }

    pub fn build(&self) -> Result<Book, BookErr> {
        let time =
            DateTime::<FixedOffset>::parse_from_str(&self.last_opened, "%Y-%m-%d %H:%M:%S %z")?;
        // let id;
        // match self.id.clone() {
        //     Some(selfid) => id = selfid,
        //     None => {
        //         let bytes = rand::thread_rng().gen::<[u8; 16]>();
        //         id = uuid::Uuid::from_bytes(bytes).to_string();
        //     }
        // }
        Ok(Book::new(
            // id,
            self.filename.clone(),
            Progress::new(self.progress)?,
            time.into(),
        ))
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Book {
    // device: ,
    // hashsum (?) not sure because will be need to calculate for every book
    // id: String,
    filename: String,
    progress: Progress,
    last_opened: DateTime<Local>,
}

impl Book {
    pub fn new(
        // id: String,
        filename: String,
        progress: Progress,
        last_opened: DateTime<Local>,
    ) -> Book {
        Book {
            // id,
            filename,
            progress,
            last_opened,
        }
    }

    // pub fn get_id(&self) -> String {
    //     self.id.clone()
    // }

    pub fn get_filename(&self) -> String {
        self.filename.clone()
    }

    pub fn get_progress(&self) -> Progress {
        self.progress.clone()
    }

    pub fn get_last_opened(&self) -> DateTime<Local> {
        self.last_opened.clone()
    }
}

#[derive(Error, Debug)]
pub enum ProgressErr {
    #[error("invalid format (expected 0.xxxxxxxx, found {found:?})")]
    WrongFormat { found: String },
}

/// book progress in persents like 0.xxxxxxxx
#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub struct Progress {
    /// book progress in persents like 0.xxxxxxxx
    val: f64,
}

impl Progress {
    /// book progress in persents like 0.xxxxxxxx
    pub fn new(val: f64) -> Result<Progress, ProgressErr> {
        if val > 1_f64 {
            return Err(ProgressErr::WrongFormat {
                found: val.to_string(),
            });
        }
        Ok(Progress { val })
    }

    pub fn new_from_pages(current_pos: f64, at_all: f64) -> Result<Progress, ProgressErr> {
        Progress::new(current_pos / at_all)
    }

    pub fn new_from_100_persents(val: f64) -> Result<Progress, ProgressErr> {
        Progress::new(val / 100_f64)
    }

    pub fn get_value(&self) -> f64 {
        self.val
    }
}
